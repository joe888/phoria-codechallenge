json.array! @card_boards do |card_board|
  json.partial! 'item', card_board: card_board
end