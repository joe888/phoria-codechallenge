class Order < ApplicationRecord
  has_many :order_items
  has_many :card_boards, :through => :order_items

  def total
    order_items_by_order_id = OrderItem.where(order_id: self.id)
    total_order_item_count = order_items_by_order_id.map(&:quantity).reduce(:+)
    total_order_price = order_items_by_order_id.map{|v| v.quantity * v.card_board.price}.reduce(:+)
    
    if total_order_item_count < 10
      total_order_price = total_order_price + 30
    elsif total_order_item_count > 20
      total_order_price = total_order_price * 0.9
    end
    self.update_columns(total_price: total_order_price)
  end

  def save_ordered_items(ordered_items)
    ordered_items.each do |ordered_item|
      parsed_data = JSON.parse ordered_item
      OrderItem.create(order_id: self.id, card_board_id: parsed_data['id'], quantity: parsed_data['quantity'])
    end
  end
end
