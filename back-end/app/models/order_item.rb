class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :card_board

  validate :order
  validate :card_board
end
