class CardBoardsController < ApplicationController
  protect_from_forgery with: :null_session
  def index
    @card_boards = CardBoard.all
  end
end
