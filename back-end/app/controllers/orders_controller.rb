class OrdersController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :validate_params
  def create
    customer_order_items = params[:order_items]
    new_order = Order.create
    new_order.save_ordered_items(customer_order_items)
    new_order.total
  end

  private
  def validate_params
  end
end
