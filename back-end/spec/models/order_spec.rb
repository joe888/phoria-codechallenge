require 'rails_helper'

RSpec.describe Order, type: :model do
  before do
    @cardboard1 = CardBoard.create(:card_board_type => 'type1', :price => 20)
    @cardboard2 = CardBoard.create(:card_board_type => 'type2', :price => 30)  
  end

  it "is valid with valid attributes" do
    expect(Order.new).to be_valid
  end

  it "should create order item" do
    order = Order.create
    order.save_ordered_items([{
      id: @cardboard1.id,
      quantity: 5
    }.to_json, {
      id: @cardboard2.id,
      quantity: 10
    }.to_json])
    OrderItem.count.should eq 2
  end

  it "should calculate total price" do
    order = Order.create
    order.save_ordered_items([{
      id: @cardboard1.id,
      quantity: 5
    }.to_json, {
      id: @cardboard2.id,
      quantity: 10
    }.to_json])
    order.total
    order.total_price.should eq 400
  end
end
