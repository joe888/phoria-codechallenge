require 'rails_helper'

RSpec.describe do
  # get cardboards
  describe 'GET /card-boards.json' do
    it 'should return all cardboards' do
      cardboard1 = CardBoard.create(:card_board_type => 'type1')
      cardboard2 = CardBoard.create(:card_board_type => 'type2')
      get "/card-boards.json", {}
      response.should be_success
      result = JSON.parse response.body
      result.length.should eq 2
    end
  end

  # create order
  describe 'POST /orders' do
    it 'shoud create orders' do
      cardboard1 = CardBoard.create(:card_board_type => 'type1', :price => 10)
      cardboard2 = CardBoard.create(:card_board_type => 'type2', :price => 20)
      post "/orders", {params: {order_items: [{id: cardboard1.id, quantity: 10}.to_json, {id: cardboard2.id, quantity: 20}.to_json]}}
      response.should be_success   
    end
  end

end
