class CreateOrderItems < ActiveRecord::Migration[5.1]
  def change
    create_table :order_items do |t|
      t.belongs_to    :card_board, index: true
      t.belongs_to    :order, index: true
      t.integer       :quantity, default: 0
      t.timestamps
    end
    add_index :order_items, [:card_board_id, :order_id], unique: true
  end
end
