Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get  '/card-boards',    to: 'card_boards#index',    as: :all_card_boards
  post '/orders',         to: 'orders#create',        as: :create_order 
end
