### Back-end
#### Requirements
* Ruby 2.4.x
* Rails 5.1

To install dependencies use bundle(ensure you are under back-end directory):

```sh
bundle
```

Then start server:

```sh
rails s
```

#### Test

```sh
rspec spec
```