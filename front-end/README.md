### Front-end
#### Requirements

* Node 6.12.x

To install dependencies use npm or yarn(ensure you are under front-end directory):
##### NPM
```sh
npm install
```

Then start server:

```sh
npm start
```

##### Yarn
```sh
yarn
```

Then start server:

```sh
yarn start
```

#### Test(Jest)
```sh
npm test
```
or
```sh
yarn test
```