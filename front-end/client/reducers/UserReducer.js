import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const authPersistConfig = {
  key: 'UserReducer',
  storage,
  blacklist: []
}

let initState = {

}
const userReducer = (state = initState, action) => {
  switch (action.type) {
    default:
      return state
  }
}

export default persistReducer(authPersistConfig, userReducer)