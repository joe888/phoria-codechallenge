import { combineReducers } from 'redux';
import DashboardReducer from './DashboardReducer';
import UserReducer from './UserReducer';

const appReducer = combineReducers({
  DashboardReducer,
  UserReducer
});

export default appReducer;