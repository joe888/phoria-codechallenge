import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import _ from 'lodash';

const dashboardPersistConfig = {
  key: 'DashboardReducer',
  storage,
  blacklist: ['cardBoards', 'err', 'isFetchingCardBoards', 'isSendingOrder']
}

let initState = {
  // card boards
  cardBoards: [],
  isFetchingCardBoards: false,
  isSendingOrder: false,
  err: null
}
const dashboardReducer = (state = initState, action) => {
  const stateWrapper = (nextState) => {
    return Object.assign({}, state, nextState)
  }
  switch (action.type) {
  	case 'FETCHING_CARDBOARDS_PENDING':
  		return stateWrapper({isFetchingCardBoards: true})
  	case 'FETCHING_CARDBOARDS_REJECTED':
  		return stateWrapper({isFetchingCardBoards: false, err: action.err})
    case 'RECEIVE_CARDBOARDS':
      let _cardBoards = [].concat(action.card_boards);
      _cardBoards.forEach(item => item.quantity = 0);
  		return stateWrapper({
        isFetchingCardBoards: false, 
        cardBoards: _cardBoards
      })
    case 'QUANTITY_CHANGE':
      let newBoards = [].concat(state.cardBoards);
      newBoards.forEach(cardBoard => {
        if(cardBoard.id === action.id) {
          cardBoard.quantity = parseInt(action.quantity, 10);
        }
      });
      return stateWrapper({cardBoards: newBoards})
    case 'ORDER_SUCCESS':
      return stateWrapper({isSendingOrder: false})
    case 'ORDER_FAILURE':
      return stateWrapper({isSendingOrder: false, err: action.err})
    case 'ORDER_PENDING':
      return stateWrapper({isSendingOrder: true})
    default:
      return state
  }
}

export default persistReducer(dashboardPersistConfig, dashboardReducer)