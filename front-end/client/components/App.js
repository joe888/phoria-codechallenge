import React, { Component } from 'react';
import './App.css';
import MyDashboard from '../containers/MyDashboard';
import GlobalHeaderContainer from '../containers/GlobalHeaderContainer';
import { 
  Container
} from 'semantic-ui-react';
import {
  HashRouter as Router,
  Route,
  Link
} from 'react-router-dom';

const BotFactory = () => (
  <Router>
    <div>
      <GlobalHeaderContainer />
      <Route exact path="/" component={Dashboard}/>
    </div>
  </Router>
)

const Dashboard = () => (
  <div className="Container">  
    <MyDashboard />
  </div>
)

export default BotFactory;
