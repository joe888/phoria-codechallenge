import React, { Component } from 'react';
import { 
  Menu
} from 'semantic-ui-react';

export default class GlobalHeader extends Component {
  componentDidMount() {

  }
  render() {
    return (
      <div className="GlobalHeader">
        <Menu inverted fixed='top' size='large' color='blue'>
          <Menu.Item name='home' active={true} children={<span>Cardboards Sale</span>}/>
        </Menu>
      </div>
    )
  }
}
