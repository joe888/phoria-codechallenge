import React, { Component } from 'react';
import { 
  Button,
  Segment,
  Header,
  Table,
  Input
} from 'semantic-ui-react';
import _ from 'lodash';

// render card board list
function CardBoardsList(props) {
  const { 
    cardBoards
  } = props;
  let renderedTemplate = <Segment>No data</Segment>
  if (cardBoards.length > 0) {
    renderedTemplate =  <Segment style={{height: '350px', overflowY: 'scroll'}}>
                          <Table celled padded>
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>Type</Table.HeaderCell>
                                <Table.HeaderCell>Price</Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>
                              {cardBoards.map(cardBoard => <CardBoardItem key={cardBoard.id} cardBoard={cardBoard}/>)}
                            </Table.Body>
                          </Table>
                        </Segment>
  }
  return renderedTemplate
}

// card board item
function CardBoardItem(props) {
  const {
    cardBoard
  } = props;
  return  <Table.Row className={`cardboard-${cardBoard.id}`}>
            <Table.Cell>{cardBoard.type}</Table.Cell>
            <Table.Cell>{cardBoard.price}</Table.Cell>
          </Table.Row>
}


function ShoppingCartItem(props) {
  const {
    cardBoard,
    quantityChange
  } = props;
  return  <Table.Row className={`cardboard-${cardBoard.id}`}>
            <Table.Cell>{cardBoard.type}</Table.Cell>
            <Table.Cell children={<Input type='number' min='0' placeholder='0' onChange={(e)=>{quantityChange(cardBoard.id, e.target.value)}}/>}></Table.Cell>
          </Table.Row>
}

function ShoppingCart(props) {
  const {
    cardBoards,
    quantityChange,
    sendOrder
  } = props;
  return  <Segment>
            <Table celled padded>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Type</Table.HeaderCell>
                  <Table.HeaderCell>Quantity</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
              {cardBoards.map(cardBoard => <ShoppingCartItem key={cardBoard.id} cardBoard={cardBoard} quantityChange={quantityChange}/>)}
              </Table.Body>
            </Table>
            <Button onClick={()=>{sendOrder(cardBoards)}}>Submit Order</Button>
          </Segment>
}


class Dashboard extends Component {
	componentDidMount() {
    this.props.fetchCardBoards();
	}

  render() {
  	const {
      cardBoards,
      fetchCardBoards,
      quantityChange,
      sendOrder,
      isSendingOrder,
      err
    } = this.props;

    return (
      <div className="Dashboard">
        <Segment>
          <Header>All CardBoards ({cardBoards.length})</Header>
          <CardBoardsList cardBoards={cardBoards}/>
        </Segment>
        <Segment>
          <Header>Shopping Cart</Header>
          <ShoppingCart cardBoards={cardBoards} quantityChange={quantityChange} sendOrder={sendOrder}/>
        </Segment>
      </div>
    ) 
  }
}

export default Dashboard;
