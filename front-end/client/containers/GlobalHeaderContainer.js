import { connect } from 'react-redux';
import GlobalHeader from '../components/GlobalHeader';

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

const GlobalHeaderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GlobalHeader);

export default GlobalHeaderContainer;