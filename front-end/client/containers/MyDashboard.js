import { connect } from 'react-redux';
import { 
  fetchCardBoards,
  quantityChange,
  sendOrder
} from '../actions';
import Dashboard from '../components/Dashboard'

const mapStateToProps = state => {
  return {
  	err: state.DashboardReducer.err,
    isFetchingCardBoards: state.DashboardReducer.isFetchingCardBoards,
    cardBoards: state.DashboardReducer.cardBoards,
    isSendingOrder: state.DashboardReducer.isSendingOrder
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    fetchCardBoards: () => {
      dispatch(fetchCardBoards());
    },
    quantityChange: (id, quantity) => {
      dispatch(quantityChange(id, quantity));
    },
    sendOrder: (items) => {
      dispatch(sendOrder(items));
    }
  }
}

const MyDashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

export default MyDashboard;