import Zjax from '../utils/zjax';
import ActionTypes from './ActionTypes';
import axios from 'axios';
var zjax = new Zjax();

// order 
function sendOrderPending() {
  return {
    type: ActionTypes.ORDER_PENDING,
  }
}

function sendOrderFulfilled() {
  return {
    type: ActionTypes.ORDER_SUCCESS,
  }
}

function sendOrderRejected(err) {
  return {
    type: ActionTypes.ORDER_FAILURE,
    err: err
  }
}

export const sendOrder = (items) => {
  return function (dispatch) {
    dispatch(sendOrderPending());
    console.log(items)
    console.log('###', items.filter(item => item.quantity > 0))
    zjax.request({
      url: `/orders`,
      option: {
        method: 'post',
        params: {
          order_items: items.filter(item => item.quantity > 0).map(item => {
            return {
              id: item.id,
              quantity: item.quantity
            }
          })
        }
      },
      successCallback: (response) => {
        dispatch(sendOrderFulfilled());
      },
      failureCallback: (err) => {
        dispatch(sendOrderRejected(err));
      }
    })
  }  
}


// quantity change
export function quantityChange(id, quantity) {
  return {
    type: ActionTypes.QUANTITY_CHANGE,
    id: id,
    quantity: quantity
  }
}


// card board
function fetchingCardBoardsPending() {
  return {
    type: ActionTypes.FETCHING_CARDBOARDS_PENDING,
  }
}

function fetchingCardBoardsRejected(err) {
  return {
    type: ActionTypes.FETCHING_CARDBOARDS_REJECTED,
    err: err
  }
}


function receiveCardBoards(json) {
  return {
    type: ActionTypes.RECEIVE_CARDBOARDS,
    card_boards: json
  }
}


export const fetchCardBoards = (option) => {
  return function (dispatch) {
    dispatch(fetchingCardBoardsPending());
    zjax.request({
      url: '/card-boards',
      option: {
        method: 'get'
      },
      successCallback: (response) => {
        dispatch(receiveCardBoards(response.data));
      },
      failureCallback: (err) => {
        dispatch(fetchingCardBoardsRejected(err));
      }
    });
  }
}