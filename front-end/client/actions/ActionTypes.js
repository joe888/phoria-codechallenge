const ActionTypes = {

  // card boards fetching
  FETCHING_CARDBOARDS_PENDING: 'FETCHING_CARDBOARDS_PENDING',
  FETCHING_CARDBOARDS_REJECTED: 'FETCHING_CARDBOARDS_REJECTED',
  RECEIVE_CARDBOARDS: 'RECEIVE_CARDBOARDS',

  // order
  SEND_ORDER: 'SEND_ORDER',
  ORDER_FAILURE: 'ORDER_FAILURE',
  ORDER_SUCCESS: 'ORDER_SUCCESS',
  ORDER_PENDING: 'ORDER_PENDING',

  QUANTITY_CHANGE: 'QUANTITY_CHANGE'
}

export default ActionTypes