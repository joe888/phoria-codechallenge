import React from 'react';
import DashboardReducer from '../reducers/DashboardReducer';
import ActionTypes from '../actions/ActionTypes';
import card_boards from './fixtures/card_boards';

describe('>>> DashboardReducer test', () => {
  it('+++ should return the initial state', () => {
    expect(DashboardReducer(undefined, {})).toEqual({
      cardBoards: [],
      isFetchingCardBoards: false,
      isSendingOrder: false,
      err: null
    });
  });

  it('+++ should handle fetchingCardBoardsPending', () => {
    const _fetchCardBoardsPending = {
      type: ActionTypes.FETCHING_CARDBOARDS_PENDING
    };
    expect(DashboardReducer({}, _fetchCardBoardsPending)).toEqual({
      isFetchingCardBoards: true
    });
  });

  it('+++ should handle fetchingCardBoardsRejected', () => {
    const _fetchCardBoardsRejected = {
      type: ActionTypes.FETCHING_CARDBOARDS_REJECTED,
      err: 'err'
    };
    expect(DashboardReducer({}, _fetchCardBoardsRejected)).toEqual({
    	err: 'err',
    	isFetchingCardBoards: false,
    });
  });

  it('+++ should handle receiveCardBoards', () => {
    const _receiveCardBoards = {
      type: ActionTypes.RECEIVE_CARDBOARDS,
      card_boards: card_boards
    };
    expect(DashboardReducer({
    	cardBoards: []
    }, _receiveCardBoards)).toEqual({
      "isFetchingCardBoards": false,
    	"cardBoards": [{"id": 1, "price": 20, "quantity": 0, "type": "high"}, {"id": 2, "price": 30, "quantity": 0, "type": "premium"}]
    });
  });
 

  it('+++ should handle sendOrderPending', () => {
    const _sendOrderPending = {
      type: ActionTypes.ORDER_PENDING
    };
    expect(DashboardReducer({}, _sendOrderPending)).toEqual({
    	isSendingOrder: true
    });
  });

  it('+++ should handle sendOrderFulfilled', () => {
    const _sendOrderFulfilled = {
      type: ActionTypes.ORDER_SUCCESS
    };
    expect(DashboardReducer({}, _sendOrderFulfilled)).toEqual({
    	isSendingOrder: false
    });
  });

  it('+++ should handle sendOrderRejected', () => {
    const _sendOrderRejected = {
      type: ActionTypes.ORDER_FAILURE,
      err: 'error'
    };
    expect(DashboardReducer({}, _sendOrderRejected)).toEqual({
    	isSendingOrder: false,
      err: 'error'
    });
  });

});