import React from 'react';
import Dashboard from '../components/Dashboard';
import {shallow, mount} from 'enzyme';
import renderer from 'react-test-renderer';
import card_boards from './fixtures/card_boards';

// snapshot test
test('>>> Dashboard test', () => {

  const component = renderer.create(
    <Dashboard cardBoards={card_boards} fetchCardBoards={()=>{}}/>,
  );
  let tree = component.toJSON();

  expect(tree).toMatchSnapshot();
});

describe('>>> Shallow Render Dashboard Component', () => {
  let wrapper = null,
      newState = {},
      initState = {
        cardBoards: card_boards,
        isFetchingCardBoards: false,
        isSendingOrder: false,
        fetchCardBoards: ()=>{},
        err: null
      };

  wrapper = mount(<Dashboard {...initState}/>)

  it('+++ render the component', () => {
    expect(wrapper.find(Dashboard).length).toEqual(1);
  });
});
